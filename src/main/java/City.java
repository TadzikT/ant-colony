import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

class City extends Point2D {

    private static int size = 8;
    private static Color color = Color.BLUE;

    City(int x, int y) {
        super(x, y);
    }

    static int getSize() {
        return size;
    }

    static Color getColor() {
        return color;
    }

}
