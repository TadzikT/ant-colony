import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.jgrapht.UndirectedGraph;

import java.util.Random;

class View {
/*
    private Canvas canvas;
    private UndirectedGraph<City, Road> graph;

    public View(Canvas canvas, UndirectedGraph<City, Road> graph) {
        this.canvas = canvas;
        this.graph = graph;
    }
*/
    private Random random = new Random();

    void drawCities(Canvas canvas, UndirectedGraph<City, Road> graph) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(City.getColor());
        for (City city : graph.vertexSet()) {
            gc.fillRect(city.getX() - City.getSize() / 2, city.getY() - City.getSize() / 2, City.getSize(),
                    City.getSize());
        }
    }

    void drawRoads(Canvas canvas, UndirectedGraph<City, Road> graph) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(0.3);
        gc.setStroke(Color.BLACK);
        int lineCount = 0;
        for (Road road : graph.edgeSet()) {
            lineCount++;
            gc.strokeLine(graph.getEdgeSource(road).getX(), graph.getEdgeSource(road).getY(),
                    graph.getEdgeTarget(road).getX(), graph.getEdgeTarget(road).getY());
        }
        System.out.println("Lines drawn: " + lineCount);
    }

    void clearCanvas(Canvas canvas) {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    // it will be drawn on the top of drawn roads, maybe not okay, but it doesn't have to redraw roads not included in
    // this tour.
    void drawShortestTour(Canvas canvas, Ant ant) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(4);
        gc.setStroke(getRandomPrettyColor());
        for (int i = 0; i < ant.getVisitedCities().size() - 1; i++) {
            gc.strokeLine(ant.getVisitedCities().get(i).getX(), ant.getVisitedCities().get(i).getY(),
                    ant.getVisitedCities().get(i + 1).getX(), ant.getVisitedCities().get(i + 1).getY());
        }
    }

    private Color getRandomPrettyColor() {
        float hue = random.nextInt(360);
        // Saturation between 0.1 and 0.3
        float saturation = (random.nextInt(2000) + 1000) / 10000f;
        float luminance = 0.9f;
        return Color.hsb(hue, saturation, luminance);
    }

}
