import javafx.scene.canvas.Canvas;
import javafx.util.Pair;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;

import java.util.*;

class Model {

    private Canvas canvas;
    private View view = new View();
    private UndirectedGraph<City, Road> graph;
    private Random random = new Random();
    private List<Ant> ants = new ArrayList<>();
    private double roadsInitialPheromoneStrength;

    Model(Canvas canvas) {
        this.canvas = canvas;
    }

    void createCities(int numberOfCities, int cityMaxPosX, int cityMaxPosY) {
        if (numberOfCities < 2) {
            throw new RuntimeException("Number of cities must be at least 2.");
        }
        graph  = new SimpleGraph<>(Road.class);
        for (int i = 0; i < numberOfCities; i++) {
            graph.addVertex(new City(random.nextInt(cityMaxPosX), random.nextInt(cityMaxPosY)));
        }
    }

    void createRoads(double roadInitialPheromoneStrength) {
        this.roadsInitialPheromoneStrength = roadInitialPheromoneStrength;
        for (City city1 : graph.vertexSet()) {
            for (City city2 : graph.vertexSet()) {
                if (city1 != city2) {
                    graph.addEdge(city1, city2, new Road(this.roadsInitialPheromoneStrength, city1.distance(city2)));
                }
            }
        }
        // view.drawRoads(canvas, graph);
        view.drawCities(canvas, graph);
    }

    void placeAnts(int numberOfAnts) {
        if (numberOfAnts < 1) {
            throw new RuntimeException("Number of ants must be at least 1.");
        }
        ants.clear();
        // tfw graph return vertex as a Set, and Set doesn't have get() method
        for (int i = 0; i < numberOfAnts; i++) {
            int index = random.nextInt(graph.vertexSet().size());
            Iterator<City> iter = graph.vertexSet().iterator();
            for (int j = 0; j < index; j++) {
                iter.next();
            }
            Ant ant = new Ant(new ArrayList<>());
            ant.getVisitedCities().add(iter.next());
            ants.add(ant);
        }
    }

    void simulateAnts(double a, double b, double q) {
        // using graph.vertexSet().size() is less optimal, but taking numberOfCities (which then should
        // be saved as Model class member) might be more bug prone, like when graph gets updates with more Cities but
        // numberOfCities stays the same
        for (int i = 0; i < graph.vertexSet().size(); i++) {
            iterateAnts(a, b, q);
        }
    }

    private void iterateAnts(double a, double b, double q) {
        // not sure what these a and b are, lol. they are alpha and beta actually.
        // b (beta) - "determines the relative importance of pheromone density versus distance"
        // it has a value of 0.6 in a presentation i've found, but higher values gives better results.
        // Values a little higher than 100, will produce bugs in simulation.
        for (Ant ant : ants) {
            // dla lepszej optymalizacji mozna by uzyc tablicy o wielkosci tyle ile jest drog. tylko wtedy trzeba
            // ponizsza petle zmienic na ta tradycyjna ze srednikami i w ogole trudniej jest. wtedy zamiast obiektu
            // Double mozna uzywac w tej tablicy i ponizsze obiektu Double tez to mozna uzywac double (primitive type)
            // i bedzie szybciej.
            List<Pair<Road, Double>> probabilisticValues = new ArrayList<>();
            City currentCityOfAnAnt = ant.getVisitedCities().get(ant.getVisitedCities().size() - 1);
            // znajduje kolekcje drog ktore moze wybrac mrowka. nie moze byc w nich miasta, w ktorym juz byla ta
            // mrowka, chyba ze to ostatnie miasto (te z ktorego mrowka na poczatku wyszla).
            Set<Road> roadsAvailableForAnAnt = new HashSet<>();
            for (City city1 : graph.vertexSet()) {
                boolean okayToAdd = true;
                for (City city2 : ant.getVisitedCities()) {
                    if (city1 == city2) {
                        okayToAdd = false;
                        break;
                    }
                }
                if (okayToAdd) {
                    roadsAvailableForAnAnt.add(graph.getEdge(currentCityOfAnAnt, city1));
                }
            }
            // dla optymalizacji najlepiej to pomin te obliczenia ponizej i po prostu powiedz mrowce zeby odrazu
            // wybrala ta jedyna droge. ale na razie mam ze nie pomija obliczen, i dodaje tutaj do hashseta ta jedna
            // mozliwa droge.
            if (roadsAvailableForAnAnt.isEmpty() && ant.getVisitedCities().size() <= graph.vertexSet().size()) {
                roadsAvailableForAnAnt.add(graph.getEdge(currentCityOfAnAnt, ant.getVisitedCities().get(0)));
            }
            for (Road road1 : roadsAvailableForAnAnt) {
                // probabilisticValueOfThisRoad dopisz do jakies listy wartosci z kazdych drog tego.
                // zoptymalizowac to? ta czesc (0.2)^0.2 * (1/100)^0.6 moze byc liczona raz na drogę, a nie ze przy
                // liczeniu kazdej drogi to sie jeszcze raz liczy do kazdej drogi i potem dodaje i dzieli zeby dostac
                // probabilistic value. CHYBA, bo wartosc feromonow sie zmienia, moze a i b tez sie zmieniaja, wiec
                // wtedy to ciezko tak zoptymalizowac.
                Double FirstPartOfAProbabilisticValueOfThisRoad = (Math.pow(road1.getPheromoneStrength(), a) *
                        Math.pow(1 / road1.getDistance(), b));
                Double SecondPartOfAProbabilisticValueOfThisRoad = 0.0;
                for (Road road2 : roadsAvailableForAnAnt) {
                    SecondPartOfAProbabilisticValueOfThisRoad += (Math.pow(road2.getPheromoneStrength(), a) *
                            Math.pow(1 / road2.getDistance(), b));
                }
                probabilisticValues.add(new Pair<>(road1, FirstPartOfAProbabilisticValueOfThisRoad
                        / SecondPartOfAProbabilisticValueOfThisRoad));
            }
            Double randomNumber = random.nextDouble();
            Double sumOfValues = 0.0;
            for (Pair<Road, Double> probabilisticValue : probabilisticValues) {
                sumOfValues += probabilisticValue.getValue();
                if (randomNumber <= sumOfValues) {
                    Road chosenRoad = probabilisticValue.getKey();
                    ant.getVisitedCities().add(graph.getEdgeTarget(chosenRoad) != currentCityOfAnAnt ?
                            graph.getEdgeTarget(chosenRoad) : graph.getEdgeSource(chosenRoad));
                    ant.setTourDistance(ant.getTourDistance() + chosenRoad.getDistance());
                    updateLocalPheromones(q, chosenRoad, ant);
                    break;
                }
                if (probabilisticValue == probabilisticValues.get(probabilisticValues.size() - 1)) {
                    resetRoadsPheromones();
                    throw new RuntimeException("Error: Can't find suitable road for an ant. Alpha or beta value too " +
                            "big?");
                }
            }
        }
        updateGlobalPheromones(graph.edgeSet()); // not sure if it should be here?
    }

    // this method maybe should be separated into few methods
    void showToursDistance() {
        Ant antWithAShortestTour = ants.get(0); // this is probably not okay, but assigning to null seems worse
        for (Ant ant : ants) {
            if (ant.getTourDistance() <= antWithAShortestTour.getTourDistance()) {
                antWithAShortestTour = ant;
            }
            System.out.println(ant.getTourDistance());
        }
        System.out.println("Shortest path: " + antWithAShortestTour.getTourDistance());
        view.drawShortestTour(canvas, antWithAShortestTour);
    }

    private void updateLocalPheromones(double q, Road chosenRoad, Ant ant) {
        // don't know why q is 100 by default
        chosenRoad.setPheromoneStrength(chosenRoad.getPheromoneStrength() + q / ant.getTourDistance());
    }

    private void updateGlobalPheromones(Set<Road> roads) {
        double p = 0.6;
        // p is 0.6 by default. again, this number was just in a presentation without explaining why it has this value
        // higher value than 0.6 will make the tours unfinished, much higher value will make no tour to be finded
        for (Road road : roads) {
            road.setPheromoneStrength(road.getPheromoneStrength() * (1 - p));
        }
    }

    void resetRoadsPheromones() {
        for (Road road : graph.edgeSet()) {
            road.setPheromoneStrength(roadsInitialPheromoneStrength);
        }
    }

    View getView() {
        return view;
    }

}