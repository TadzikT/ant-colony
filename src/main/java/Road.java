import org.jgrapht.graph.DefaultEdge;

class Road extends DefaultEdge{

    private double distance;
    private double pheromoneStrength;

    Road(double pheromoneStrength, double distance) {
        this.pheromoneStrength = pheromoneStrength;
        this.distance = distance;
    }

    double getDistance() {
        return distance;
    }

    double getPheromoneStrength() {
        return pheromoneStrength;
    }

    void setPheromoneStrength(double pheromoneStrength) {
        this.pheromoneStrength = pheromoneStrength;
    }

}