// import javafx.scene.paint.Color;

import java.util.List;

class Ant {

    private List<City> visitedCities;
    private double tourDistance = 0.0;

    Ant(List<City> visitedCities) {
        this.visitedCities = visitedCities;
    }

    List<City> getVisitedCities() {
        return visitedCities;
    }

    double getTourDistance() {
        return tourDistance;
    }

    void setTourDistance(double tourDistance) {
        this.tourDistance = tourDistance;
    }

}
