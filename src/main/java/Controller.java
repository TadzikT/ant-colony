import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;

import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Canvas canvas;
    @FXML
    private TextField numberOfCitiesTextField;
    @FXML
    private TextField initialPheromonesTextField;
    @FXML
    private TextField numberOfAntsTextField;
    @FXML
    private TextField valueOfAlphaTextField;
    @FXML
    private TextField valueOfBetaTextField;
    @FXML
    private TextField valueOfQTextField;
    @FXML
    private Button startSimulationButton;
    private Model model;
    private ChangeListener<String> allowIntegersOnly = (observable, oldValue, newValue) -> {
        if (!newValue.matches("\\d*")) {
            ((StringProperty)observable).setValue(oldValue);
        }
    };
    private ChangeListener<String> allowNumbersOnly = (observable, oldValue, newValue) -> {
        if (!newValue.matches("\\d*\\.?\\d*")) {
            ((StringProperty)observable).setValue(oldValue);
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        model = new Model(canvas);
        numberOfCitiesTextField.textProperty().addListener(allowIntegersOnly);
        initialPheromonesTextField.textProperty().addListener(allowNumbersOnly);
        numberOfAntsTextField.textProperty().addListener(allowIntegersOnly);
        valueOfAlphaTextField.textProperty().addListener(allowNumbersOnly);
        valueOfBetaTextField.textProperty().addListener(allowNumbersOnly);
        valueOfQTextField.textProperty().addListener(allowNumbersOnly);
        setDefaultTextfields();
    }

    @FXML
    public void createCitiesAndRoads() {
        model.getView().clearCanvas(canvas);
        model.createCities(Integer.parseInt(numberOfCitiesTextField.getText()), (int) canvas.getWidth(),
                (int) canvas.getHeight());
        model.createRoads(Double.parseDouble(initialPheromonesTextField.getText()));
        startSimulationButton.setDisable(false);
    }

    @FXML
    public void startSimulation() {
        model.placeAnts(Integer.parseInt(numberOfAntsTextField.getText()));
        model.simulateAnts(Double.parseDouble(valueOfAlphaTextField.getText()),
                Double.parseDouble(valueOfBetaTextField.getText()), Double.parseDouble(valueOfQTextField.getText()));
        model.showToursDistance();
        model.resetRoadsPheromones();
    }

    @FXML
    public void setDefaultTextfields() {
        numberOfCitiesTextField.setText("50");
        initialPheromonesTextField.setText("0.2");
        numberOfAntsTextField.setText("100");
        valueOfAlphaTextField.setText("0.2");
        valueOfBetaTextField.setText("60");
        valueOfQTextField.setText("100");
    }
}

